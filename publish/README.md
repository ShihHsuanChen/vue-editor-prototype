# vue-editor-prototype

> This is a easy-to-render vue text editor package base on [Quill](https://quilljs.com/) (quill@1.3.7)

## Feature
- Custom toolbar layout
- Custom toolbar icons
- Custom preview page
- Can imbed the api upload/download for image resources
- Provide part of Quill API for custom event listeners

## Disadvantage
- Cannot use Quill custom modules and further Quill settings

## Install as a npm package

### Install by default branch (master)

``` bash
npm install --save https://bitbucket.org/ShihHsuanChen/vue-editor-prototype.git
```

### Install the specify tag/branch version

``` bash
# for example: tag v1.0.01
npm install --save https://bitbucket.org/ShihHsuanChen/vue-editor-prototype.git#v1.0.01
```

## Import vue-editor-prototype

### Import in main.js
``` js
import Vue from 'vue'
import VueEditorPrototype from 'vue-editor-prototype'
import 'vue-editor-prototype/publish/static/vue-editor-prototype.css'

Vue.use(VueEditorPrototype)
```

### Import in your component (.vue)
``` html
<template>
<div>
  <editor
    v-model="content"
    v-slot:default="editor"
    useMarkdownShortcuts>
    <editor-toolbar
      :useDefaultTitle="true"
      :editor="editor">
      <editor-toolbar-item func="header"/>
      <editor-toolbar-item func="size"/>
      <editor-toolbar-item func="font"/>
      <editor-toolbar-item func="bold"/>
      <editor-toolbar-item func="italic"/>
      <editor-toolbar-item func="underline"/>
      <editor-toolbar-item func="strike"/>
    </editor-toolbar>
    <editor-content :editor=editor></editor-content>
  </editor>
  <div>
    <editor-preview></editor-preview>
  </div>
</div>
</template>

<script>
import {
  Editor,
  EditorToolbar,
  EditorToolbarItem,
  EditorContent,
  EditorPreview
} from 'vue-editor-prototype'

export default {
  components: {
    Editor,
    EditorToolbar,
    EditorToolbarItem,
    EditorContent,
    EditorPreview
  },
  data () {
    return {
      content: '<h1>Initial Content</h1>'
    }
  },
  ...
}
</script>

<style src='vue-editor-prototype/publish/static/vue-editor-prototype.css'></style>
```

## Components

### Editor Attributes

Attribute | Description | Type | Accepted Values | Default
:-------- | :---------- | :--- | :------------- | :------
value / v-model | text content | string / text with html tags | --
useMarkdownShortcuts | input text with markdown shortcurts | boolean | true / false | false

### Editor Slots

Name | Description | Parameters
:--- | :---------- | :---------
(default) | default slot | { editor: object }

### Editor Events

Event Name | Description | Parameters
:--------- | :---------- | :--------
focus | triggers when edit area focuses | (quill: object)
blur | triggers when edit area blurs | (quill: object)
text-change | triggers when input text changes | (content: string)
selection-change | triggers when selected range changes | (quill: object)
editor-change | triggers when either text-change or selection-change is triggered | (obj: object)
image-added | triggers when image is selected from image selector | (file: object, insertEmbed: Function)
image-removed | trigger when image is removed | (image: object)

### Editor Toolbar Attributes
Attribute | Description | Type | Accepted Values | Default
:-------- | :---------- | :--- | :------------- | :------
editor | slot parameter from Editor default slot | object | - | -
useDefaultTitle | apply default title on icons | string / boolean | true / false / 'en' / 'zh' | true

### Editor Toolbar Item Attributes
Attribute | Description | Type | Accepted Values | Default
:-------- | :---------- | :--- | :------------- | :------
func | function name | string | function names (see DemoFull.vue) | -
useDefaultTitle | apply default title on icons | string / boolean | true / false / 'en' / 'zh' | true

### Editor Content Attributes
Attribute | Description | Type | Accepted Values | Default
:-------- | :---------- | :--- | :------------- | :------
(empty) |

### Editor Preview Attributes
Attribute | Description | Type | Accepted Values | Default
:-------- | :---------- | :--- | :------------- | :------
content | value / v-model of Editor attribute | string | - | -
paper | setting paper size / padding (only for view) | string / object | A4 / { height, width, padding: { top, left, right, bottom } } | 'A4'


## View Demo

``` bash
# clone the repository
git clone https://ShihHsuanChen@bitbucket.org/ShihHsuanChen/vue-editor-prototype.git

# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev
```

## Build Setup

``` bash
# build for production with minification
npm run build

# build for package publish
npm run build-publish

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test
```
For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

## License
See LICENSE
