import request from './https.js'

export const API = {
  upload: (name, file, fmt) => {
    let formData = new FormData()
    formData.append('AN', '999')
    formData.append('series_no', '0')
    formData.append('image_no', '0')
    formData.append('tag', name)
    formData.append('format', fmt)
    formData.append('image', file)
    return request('post', '/upload', formData)
  }
}
