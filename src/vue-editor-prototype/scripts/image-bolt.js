import Quill from 'quill'

const Image = Quill.import('formats/image')

class ImageBlot extends Image {
  static create (value) {
    const node = super.create()
    node.setAttribute('src', value.url)
    node.setAttribute('id', value.id)
    return node
  }

  static value (node) {
    return {
      url: node.getAttribute('src'),
      id: node.getAttribute('id')
    }
  }
}

ImageBlot.blotName = 'image'
ImageBlot.tagName = 'img'

export { ImageBlot as default }
