import { Buffer } from 'buffer'
import axios from 'axios'

export function getAttr (obj, attrName, defaultVal) {
  if (obj.hasOwnProperty(attrName)) return obj[attrName]
  else return defaultVal
}

export async function imgBase64Converter (elements) {
  for (let i = 0; i < elements.length; i++) {
    let element = elements[i]
    if (element.tagName !== 'IMG') throw Error('input element is not img')
    let res = await axios.get(element.src, {
      responseType: 'arraybuffer'
    })
    let buff = Buffer.from(res.data, 'binary')
    let str = buff.toString('base64')
    element.src = 'data:image/jpg;base64,' + str
  }
}

export function replaceImgs (element) {
  let imgs = element.getElementsByTagName('img')
  return imgBase64Converter(imgs)
}

class CSSRule {
  ELEMENT_RE = /[\w-]+/g
  ID_RE = /#[\w-]+/g
  CLASS_RE = /\.[\w-]+/g
  ATTR_RE = /\[[^\]]+\]/g
  PSEUDO_CLASSES_RE = /:(?!not)[\w-]+(\(.*\))?/g
  PSEUDO_ELEMENTS_RE = /::?(after|before|first-letter|first-line|selection)/g

  // convert an array-like object to array
  _toArray (list) {
    return [].slice.call(list)
  }

  // handles extraction of `cssRules` as an `Array` from a stylesheet or something that behaves the same
  _getSheetRules (stylesheet) {
    let sheetMedia = stylesheet.media && stylesheet.media.mediaText
    // if this sheet is disabled skip it
    if (stylesheet.disabled) return []
    // if this sheet's media is specified and doesn't match the viewport then skip it
    if (sheetMedia && sheetMedia.length && !window.matchMedia(sheetMedia).matches) return []
    // get the style rules of this sheet
    return this._toArray(stylesheet.cssRules)
  }

  _find (string, re) {
    let matches = string.match(re)
    return matches ? matches.length : 0
  }

  // calculates the specificity of a given `selector`
  _calculateScore (selector) {
    let score = [0, 0, 0]
    let parts = selector.split(' ')
    let part
    let match
    // TODO: clean the ':not' part since the last ELEMENT_RE will pick it up
    while (typeof part === 'string') {
      // find all pseudo-elements
      match = this._find(part, this.PSEUDO_ELEMENTS_RE)
      score[2] += match
      // and remove them
      match && (part = part.replace(this.PSEUDO_ELEMENTS_RE, ''))
      // find all pseudo-classes
      match = this._find(part, this.PSEUDO_CLASSES_RE)
      score[1] += match
      // and remove them
      match && (part = part.replace(this.PSEUDO_CLASSES_RE, ''))
      // find all attributes
      match = this._find(part, this.ATTR_RE)
      score[1] += match
      // and remove them
      match && (part = part.replace(this.ATTR_RE, ''))
      // find all IDs
      match = this._find(part, this.ID_RE)
      score[0] += match
      // and remove them
      match && (part = part.replace(this.ID_RE, ''))
      // find all classes
      match = this._find(part, this.CLASS_RE)
      score[1] += match
      // and remove them
      match && (part = part.replace(this.CLASS_RE, ''))
      // find all elements
      score[2] += this._find(part, this.ELEMENT_RE)
      part = parts.shift()
    }
    return parseInt(score.join(''), 10)
  }

  // returns the heights possible specificity score an element can get from a give rule's selectorText
  _getSpecificityScore (element, selectorText) {
    let selectors = selectorText.split(',')
    let selector
    let score
    let result = 0
    while (selector) {
      if (this.matchesSelector(element, selector)) {
        score = this._calculateScore(selector)
        result = score > result ? score : result
      }
      selector = selectors.shift()
    }
    return result
  }

  _sortBySpecificity (element, rules) {
    // comparing function that sorts CSSStyleRules according to specificity of their `selectorText`
    function compareSpecificity (a, b) {
      return this._getSpecificityScore(element, b.selectorText) - this._getSpecificityScore(element, a.selectorText)
    }
    return rules.sort(compareSpecificity.bind(this))
  }

  // Find correct matchesSelector impl
  matchesSelector (el, selector) {
    let matcher = el.matchesSelector || el.mozMatchesSelector ||
      el.webkitMatchesSelector || el.oMatchesSelector || el.msMatchesSelector
    return matcher.call(el, selector)
  }

  // TODO: not supporting 2nd argument for selecting pseudo elements
  // TODO: not supporting 3rd argument for checking author style sheets only
  getMatchedCSSRules (element) {
    let styleSheets
    let sheet
    // let sheetMedia
    let rules
    let rule
    let result = []
    // get stylesheets and convert to a regular Array
    styleSheets = this._toArray(window.document.styleSheets)

    // assuming the browser hands us stylesheets in order of appearance
    // we iterate them from the beginning to follow proper cascade order
    while (sheet) {
      // get the style rules of this sheet
      rules = this._getSheetRules(sheet)
      // loop the rules in order of appearance
      while (rule) {
        if (rule.styleSheet) { // if this is an @import rule
          // insert the imported stylesheet's rules at the beginning of this stylesheet's rules
          rules = this._getSheetRules(rule.styleSheet).concat(rules)
          // and skip this rule
          continue
        } else if (rule.media) { // if there's no stylesheet attribute BUT there IS a media attribute it's a media rule
          // insert the contained rules of this media rule to the beginning of this stylesheet's rules
          rules = this._getSheetRules(rule).concat(rules)
          // and skip it
          continue
        }

        // check if this element matches this rule's selector
        if (this.matchesSelector(element, rule.selectorText)) {
          // push the rule to the results set
          result.push(rule)
        }
        rule = rules.shift()
      }
      sheet = styleSheets.shift()
    }
    // sort according to specificity
    return this._sortBySpecificity(element, result)
  }
}

export const cssHandler = new CSSRule()
