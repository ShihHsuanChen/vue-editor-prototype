import { saveAs } from 'file-saver'
import { replaceImgs } from './util.js'

export const items = {
  quill: {
    'align-center': {
      tag: 'button',
      title: { en: 'Align Center', zh: '置中對齊' },
      class: 'ql-align',
      value: 'center'
    },
    'align-right': {
      tag: 'button',
      title: { en: 'Align Right', zh: '靠右對齊' },
      class: 'ql-align',
      value: 'right'
    },
    'align-left': {
      tag: 'button',
      title: { en: 'Align Left', zh: '靠左對齊' },
      class: 'ql-align',
      value: ''
    },
    'align-justify': {
      tag: 'button',
      title: { en: 'Justify', zh: '分散對齊' },
      class: 'ql-align',
      value: 'justify'
    },
    'blockquote': {
      tag: 'button',
      title: { en: 'Quote Block', zh: '引用' },
      class: 'ql-blockquote'
    },
    'code-block': {
      tag: 'button',
      title: { en: 'Code Block', zh: '程式碼' },
      class: 'ql-code-block'
    },
    'indent-minus': {
      tag: 'button',
      title: { en: 'Outdent', zh: '減少縮排' },
      class: 'ql-indent',
      value: '-1'
    },
    'indent-plus': {
      tag: 'button',
      title: { en: 'Indent', zh: '增加縮排' },
      class: 'ql-indent',
      value: '+1'
    },
    'list-ordered': {
      tag: 'button',
      title: { en: 'Ordered List', zh: '編號' },
      class: 'ql-list',
      value: 'ordered'
    },
    'list-bullet': {
      tag: 'button',
      title: { en: 'Bullet List', zh: '項目符號' },
      class: 'ql-list',
      value: 'bullet'
    },
    'list-check': {
      tag: 'button',
      title: { en: 'Check List', zh: '清單' },
      class: 'ql-list',
      value: 'check'
    },
    'link': {
      tag: 'button',
      title: { en: 'Link', zh: '插入連結' },
      class: 'ql-link'
    },
    'image': {
      tag: 'button',
      title: { en: 'Image', zh: '插入圖片' },
      class: 'ql-image'
    },
    'video': {
      tag: 'button',
      title: { en: 'Video', zh: '插入影片' },
      class: 'ql-video'
    },
    'bold': {
      tag: 'button',
      title: { en: 'Bold', zh: '粗體' },
      class: 'ql-bold'
    },
    'italic': {
      tag: 'button',
      title: { en: 'Italic', zh: '斜體' },
      class: 'ql-italic'
    },
    'underline': {
      tag: 'button',
      title: { en: 'Underline', zh: '底線' },
      class: 'ql-underline'
    },
    'strike': {
      tag: 'button',
      title: { en: 'Strike', zh: '刪除線' },
      class: 'ql-strike'
    },
    'subscript': {
      tag: 'button',
      title: { en: 'Subscript', zh: '下標' },
      class: 'ql-script',
      value: 'sub'
    },
    'superscript': {
      tag: 'button',
      title: { en: 'Superscript', zh: '上標' },
      value: 'super',
      class: 'ql-script'
    },
    'clean': {
      tag: 'button',
      title: { en: 'Clean format', zh: '清除格式' },
      class: 'ql-clean'
    },
    'font': {
      tag: 'select',
      title: { en: 'Font', zh: '字體' },
      class: 'ql-font'
    },
    'size': {
      tag: 'select',
      title: { en: 'Size', zh: '文字大小' },
      class: 'ql-size'
    },
    'header': {
      tag: 'select',
      title: { en: 'Style', zh: '文字樣式' },
      class: 'ql-header'
    },
    'color': {
      tag: 'select',
      title: { en: 'Color', zh: '顏色' },
      class: 'ql-color'
    },
    'background': {
      tag: 'select',
      title: { en: 'Background Color', zh: '背景顏色' },
      class: 'ql-background'
    }
  },
  other: {
    'redo': {
      class: 'my-redo',
      title: { en: 'Redo', zh: '取消復原' },
      icon: 'right-arrow',
      onClick: (e, quill) => { quill.history.redo() }
    },
    'undo': {
      class: 'my-undo',
      title: { en: 'Undo', zh: '復原' },
      icon: 'left-arrow',
      onClick: (e, quill) => {
        let nUndo = quill.history.stack.undo.length
        if (nUndo > 1) { quill.history.undo() }
      }
    },
    'export-html': {
      class: 'my-export-html',
      title: { en: 'Export HTML', zh: '匯出 HTML' },
      icon: 'export-html',
      onClick: (e, quill) => {
        let newHTML = quill.getFullHTML()
        replaceImgs(newHTML).then(() => {
          let blob = new Blob(
            [newHTML.outerHTML],
            {type: 'text/html;charset=utf-8'}
          )
          saveAs(blob, 'download.html')
        })
      }
    },
    'print': {
      class: 'my-print',
      title: { en: 'Print', zh: '列印' },
      icon: 'printer',
      onClick: (e, quill) => {
        let newHTML = quill.getFullHTML()
        replaceImgs(newHTML).then(() => {
          var win = window.open('', 'win', '')
          win.document.open()
          win.document.write(newHTML.outerHTML)
          win.document.close()
          win.onload = function () {
            win.print()
            win.close()
          }
        })
      }
    }
  }
}
