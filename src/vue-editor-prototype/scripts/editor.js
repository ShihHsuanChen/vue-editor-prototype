import Quill from 'quill'
import { MarkdownShortcuts } from './markdown-shortcuts.js'
import { getAttr } from './util.js'
import { items } from './toolbar-items.js'
import ImageBlot from './image-bolt.js'

const myToolbarButtoms = items.other

export class QuillEditor {
  quill = null

  constructor (config, options) {
    /*
    config: {
      contentDom
      toolbar
      clipboard
      history
      handleTextChange
      handleSelectionChange
      handleImageRemoved
      customImageHandler
    }
    options: {
      debug: false,
      theme: 'snow',
      placeholder: this.placeholder ? this.placeholder : '',
      readOnly: this.disabled ? this.disabled : false,
      ...
    }
    */
    this.setConfig(config)
    this.registerPrototypes()
    this.registerImageBolt()
    this.setupQuillEditor(options)
  }

  setConfig (config) {
    this.toolbar = getAttr(config, 'toolbar')
    this.clipboard = getAttr(config, 'clipboard')
    this.history = getAttr(config, 'history')
    this.modules = getAttr(config, 'modules')

    this.content = getAttr(config, 'content', '')
    this.contentDom = getAttr(config, 'contentDom', null)
    this.useMarkdownShortcuts = getAttr(config, 'useMarkdownShortcuts', true)

    this.handleSelectionChange = getAttr(
      config, 'handleSelectionChange', (range, oldRange) => {})
    this.handleTextChange = getAttr(
      config, 'handleTextChange', (delta, oldContents) => {})
    this.handleImageRemoved = getAttr(
      config, 'handleImageRemoved', (delta, oldContents) => {})
    this.handleEventListeners = getAttr(
      config, 'handleEventListeners', null)
    this.customImageHandler = getAttr(
      config, 'customImageHandler', (image, callback) => {})
  }

  updateContent (newContent) {
    this.content = newContent
    this.quill.root.innerHTML = this.content
  }

  registerPrototypes () {
    Quill.prototype.getHTML = function () {
      return this.container.querySelector('.ql-editor').innerHTML
    }
    Quill.prototype.getFullHTML = function () {
      let tmpContent = this.container.cloneNode(true)
      tmpContent.classList.add('print')
      tmpContent.style.overflow = 'unset'
      let tmpEditor = tmpContent.getElementsByClassName('ql-editor')[0]
      tmpEditor.classList.add('print')
      tmpEditor.setAttribute('contenteditable', false)

      let tmpHead = document.getElementsByTagName('head')
      let head = tmpHead[0].cloneNode(true)
      let newHTML = document.createElement('html')
      let body = document.createElement('body')

      body.innerHTML = tmpContent.outerHTML
      newHTML.appendChild(head)
      newHTML.appendChild(body)
      return newHTML
    }
    Quill.prototype.getWordCount = function () {
      return this.container.querySelector('.ql-editor').innerText.length
    }
  }

  registerImageBolt () {
    Quill.register(ImageBlot)
  }

  setupQuillEditor (editorConfig) {
    editorConfig.modules = {}
    if (this.toolbar) editorConfig.modules.toolbar = this.toolbar
    if (this.history) editorConfig.modules.history = this.history
    if (this.clipboard) editorConfig.modules.clipboard = this.clipboard
    if (this.modules) editorConfig.modules = Object.assign(editorConfig.modules, this.modules)

    if (this.useMarkdownShortcuts) {
      Quill.register('modules/markdownShortcuts', MarkdownShortcuts, true)
      editorConfig.modules.markdownShortcuts = {}
    }

    this.quill = new Quill(this.contentDom, editorConfig)
    //  set content
    this.quill.root.innerHTML = this.content
    this.registerEditorEventListeners()
    this.registerMyToolbarItems()
  }

  registerEditorEventListeners () {
    this.quill.on('text-change', this.handleContentChange.bind(this))
    this.quill.on('selection-change', this.handleSelectionChange)
    this.listenForEditorEvent('text-change')
    this.listenForEditorEvent('selection-change')
    this.listenForEditorEvent('editor-change')
  }

  listenForEditorEvent (type) {
    this.quill.on(type, (...args) => {
      if (this.handleEventListeners) {
        this.handleEventListeners(type, ...args)
      }
    })
  }

  handleContentChange (delta, oldContents) {
    if (this.handleTextChange) this.handleTextChange(delta, oldContents)
    if (this.handleImageRemoved) this.handleImageRemoved(delta, oldContents)
  }

  registerMyToolbarItems () {
    let toolbarDom = this.quill.options.modules.toolbar.container
    for (let i = 0; i < Object.keys(myToolbarButtoms).length; i++) {
      let func = Object.keys(myToolbarButtoms)[i]
      let itm = myToolbarButtoms[func]
      toolbarDom.querySelectorAll('.' + itm.class).forEach(tar => {
        tar.addEventListener('click', (e) => { itm.onClick(e, this.quill) })
      })
    }
  }
}
