import Vue from 'vue'
import Router from 'vue-router'
import Menu from '@/components/Menu'
import DemoFull from '@/components/DemoFull'
import DemoSimple from '@/components/DemoSimple'
import DemoMedia from '@/components/DemoMedia'
import DemoCustomIcon from '@/components/DemoCustomIcon'
import DemoToolbarLayout from '@/components/DemoToolbarLayout'
import DemoIconTitle from '@/components/DemoIconTitle'
import DemoPreview from '@/components/DemoPreview'
import DemoStyle from '@/components/DemoStyle'
import DemoHandlers from '@/components/DemoHandlers'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Menu',
      component: Menu,
      children: [
        {
          path: '/demo-full',
          name: 'Demo Everything',
          component: DemoFull
        },
        {
          path: '/demo-simple',
          name: 'Simple Usage',
          component: DemoSimple
        },
        {
          path: '/demo-image',
          name: 'Insert Media',
          component: DemoMedia
        },
        {
          path: '/demo-custom-icon',
          name: 'Custom Toolbar Icon',
          component: DemoCustomIcon
        },
        {
          path: '/demo-custom-layout',
          name: 'Custom Toolbar Layout',
          component: DemoToolbarLayout
        },
        {
          path: '/demo-custom-title',
          name: 'Custom Icon title',
          component: DemoIconTitle
        },
        {
          path: '/demo-preview',
          name: 'Preview',
          component: DemoPreview
        },
        {
          path: '/demo-style',
          name: 'Style Override',
          component: DemoStyle
        },
        {
          path: '/demo-handlers',
          name: 'More Handlers',
          component: DemoHandlers
        }
      ]
    }
  ]
})
