import SvgIcon from 'vue-svgicon'
import Editor from './vue-editor-prototype/Editor'
import EditorContent from './vue-editor-prototype/EditorContent'
import EditorToolbar from './vue-editor-prototype/EditorToolbar'
import EditorToolbarItem from './vue-editor-prototype/EditorToolbarItem'
import EditorPreview from './vue-editor-prototype/EditorPreview'

export function install (Vue, options) {
  if (install.installed) return
  install.installed = true

  Vue.component('Editor', Editor)
  Vue.component('EditorContent', EditorContent)
  Vue.component('EditorPreview', EditorPreview)
  Vue.component('EditorToolbar', EditorToolbar)
  Vue.component('EditorToolbarItem', EditorToolbarItem)
  Vue.use(SvgIcon, { tagName: 'svgicon' })
}

const VueEditorPrototype = {
  install,
  Editor,
  EditorContent,
  EditorToolbar,
  EditorToolbarItem,
  EditorPreview
}

// Auto-install when vue is found (eg. in browser via <script> tag)
let GlobalVue = null
if (typeof window !== 'undefined') {
  GlobalVue = window.Vue
} else if (typeof global !== 'undefined') {
  GlobalVue = global.Vue
}
if (GlobalVue) {
  GlobalVue.use(VueEditorPrototype)
}

export default VueEditorPrototype
export { Editor }
